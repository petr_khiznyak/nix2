<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
<form action="addToTable.php" method="post">
    <label for="first_name">Имя</label>
    <input type="text" name="first_name" id="first_name">
    <label for="last_name">Фамилия</label>
    <input type="text" name="last_name" id="last_name">
    <label for="age">Возраст</label>
    <input type="number" name="age" id="age">
    <label for="phone_number">Телефон</label>
    <input type="text" name="phone_number" id="phone_number">
    <button type="submit">Отправить</button>
</form>
</body>
</html>