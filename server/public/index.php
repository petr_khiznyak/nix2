<?php
require_once '../vendor/autoload.php';


class Car
{
  public string $model;
  public string $color;
  public float $power;
  public int $year;
  public string $fuel;
  public float $price;
  public function __construct(string $model, string $color, float $power, int $year, string $fuel, float $price)
  {
    echo __FUNCTION__ . $model . '<br>';

    $this->model = $model;
    $this->color = $color;
    $this->power = $power;
    $this->year = $year;
    $this->fuel = $fuel;
    $this->price = $price;

  }

  public function showDetails()
  {
    echo 'Привет я купил себе ' . $this->model .
      'цвет - ' . $this->color . 'год - ' . $this->year . 'за ' . $this->price;
  }

  public function __destruct()
  {
    echo __FUNCTION__ . $this->model . '<br>';
  }
}

$bmw = new Car('bmw', 'black', 123, 2020, 'gegw', 15000);
$mazda = new Car('mazda', 'black', 123, 2020, 'gegw', 12000);
$audi = new Car('audi', 'red', 200, 2000, 'gegw', 300);

echo '<pre>';
var_dump($bmw);
echo '</pre>';
echo '<pre>';
var_dump($mazda);
echo '</pre>';
echo '<pre>';
var_dump($audi);
echo '</pre>';